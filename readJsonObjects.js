const fs = require('fs')
const contents = fs.readFileSync('filePaths.json')
// Define to JSON type
const jsonContent = JSON.parse(contents)
const removeRoutes = []
const paths = []

jsonContent.forEach((obj) => {
    removeRoutes.push(obj.removeRoute)
})
jsonContent.forEach((obj) => {
    paths.push(obj.path)
})

module.exports = {
    removeRoutes,
    paths
}