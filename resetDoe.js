const fs = require('fs')
const rimraf = require('rimraf')
const makePath = require('path');
const chalk = require('chalk') //Colorized messages aid in the readablity of the program

const $DOE_ROOT = process.env.DOE_ROOT //For the test run: $DOE_ROOT = '/rw/dex/ptf3'

const readJsonObjects = require('./readJsonObjects')
const read = require('./read.js')

async function initRead(){
    return new Promise((resolve, reject) => {
        let j = 0 //Counter so the order in which the program runs works properly
        readJsonObjects.paths.forEach((path) => {
            fs.readdir(makePath.join($DOE_ROOT + path), (err, files) => {
                read(j, err, files)
                j++
                //Only exits the function once it is totally done with iterating through the list of paths initially
                if(readJsonObjects.paths.length === j){
                    resolve()
                }
                
                
            })
        }) 
    })
    
}
async function finRead(){
    await initRead()
    await new Promise((resolve, reject) => {
        let i = 0
        readJsonObjects.removeRoutes.forEach((route) => {
            rimraf(makePath.join($DOE_ROOT + route), () => {
                i++
                console.log('Deleting files')
                if(readJsonObjects.removeRoutes.length === i){
                    resolve()
                }
            })
        
        })
    })
    await initRead()
}

if($DOE_ROOT){
    console.log(chalk.green('DOE_ROOT is equal to: ' + $DOE_ROOT))
    finRead()
} else{
    console.log(chalk.red('Please set a value for DOE_ROOT'))
}

//
//In order to remove individual files, just put the path (path)
//To delete whole directories, put the entire path, but no '/' at the end
//To take out certain file types, put the path up to the end directory, with the '/' at the end. Then, put '*.____'
//To gut a directory, put the path up to the ultimate directory, then put the '/*'
