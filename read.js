const fs = require('fs')
const chalk = require('chalk') //The colorized messages aid in the console's readability

const readJsonObjects = require('./readJsonObjects')

const read = (i, err, filesGiven, color) => {
    //console.log('read entered')
    console.log(chalk.yellow.underline(readJsonObjects.paths[i]))

    //handling error if directory is nonexistant, or if there is another reason it can't be accessed
    if (err) {
        return console.log(chalk.red.inverse.bold('Unable to scan directory: ' + err))
    }
    filesGiven.forEach((file) => {
        console.log(file)
    })
    if(filesGiven.length === 0){
        console.log(chalk.white.italic('No files or folders here'))
    }
}

module.exports = read